
const { Game } = require('./game')
const { TerminalInterface } = require('./terminalInterface')
const game = new Game(new TerminalInterface())

game.start()
