
class Game {
    constructor(ui) {
        this.ui = ui

        this.reset()

        // Bind handlers to UI so we can detect input change from the Game class
        this.ui.bindHandlers(
            this.changeDirection.bind(this),
            this.quit.bind(this),
            this.start.bind(this)
        )
    }

    reset() {
        // Set up initial state
        this.snake = []

        for (let i = 3; i >= 0; i--) {
            this.snake[3 - i] = { x: i, y: 0 }
        }

        this.dot = {}
        this.score = 0
        this.currentDirection = 'right'
        this.changingDirection = false
        this.timer = null

        // Generate the first dot before the game begins
        this.generateDot()
        this.ui.resetScore()
        this.ui.render()
    }

    changeDirection(_, key) {
        const DIRECTION_UP = 'up';
        const DIRECTION_RIGHT = 'right';
        const DIRECTION_DOWN = 'down';
        const DIRECTION_LEFT = 'left';
        if ((key.name === DIRECTION_UP || key.name === 'w') && this.currentDirection !== DIRECTION_DOWN) {
            this.currentDirection = DIRECTION_UP
        }
        if ((key.name === DIRECTION_DOWN || key.name === 's') && this.currentDirection !== DIRECTION_UP) {
            this.currentDirection = DIRECTION_DOWN
        }
        if ((key.name === DIRECTION_LEFT || key.name === 'a') && this.currentDirection !== DIRECTION_RIGHT) {
            this.currentDirection = DIRECTION_LEFT
        }
        if ((key.name === DIRECTION_RIGHT || key.name === 'd') && this.currentDirection !== DIRECTION_LEFT) {
            this.currentDirection = DIRECTION_RIGHT
        }
    }

    moveSnake() {
        if (this.changingDirection) {
            return
        }
        this.changingDirection = true;

        const DIRECTIONS = {
            up: { x: 0, y: -1 },
            down: { x: 0, y: 1 },
            right: { x: 1, y: 0 },
            left: { x: -1, y: 0 },
        }
        const head = {
            x: this.snake[0].x + DIRECTIONS[this.currentDirection].x,
            y: this.snake[0].y + DIRECTIONS[this.currentDirection].y,
        }

        this.snake.unshift(head)


        if (this.snake[0].x === this.dot.x && this.snake[0].y === this.dot.y) {
            this.score++
            this.ui.updateScore(this.score)
            this.generateDot()
        } else {
            // Otherwise, slither
            this.snake.pop()
        }
    }

    generateRandomPixelCoord(min, max) {
        return Math.round(Math.random() * (max - min) + min)
    }

    generateDot() {
        // Generate a dot at a random x/y coordinate
        this.dot.x = this.generateRandomPixelCoord(0, this.ui.gameContainer.width - 1)
        this.dot.y = this.generateRandomPixelCoord(1, this.ui.gameContainer.height - 1)

        // If the pixel is on a snake, regenerate the dot
        this.snake.forEach(segment => {
            if (segment.x === this.dot.x && segment.y === this.dot.y) {
                this.generateDot()
            }
        })
    }

    drawSnake() {
        // Render each snake segment as a pixel
        this.snake.forEach(segment => {
            this.ui.draw(segment, 'white')
        })
    }

    drawDot() {
        // Render the dot as a pixel
        this.ui.draw(this.dot, 'white')
    }

    isGameOver() {
        // If the snake collides with itself, end the game
        const collide = this.snake
            // Filter out the head
            .filter((_, i) => i > 0)
            // If head collides with any segment, collision
            .some(segment => segment.x === this.snake[0].x && segment.y === this.snake[0].y)

        return (
            collide ||
            // Right wall
            this.snake[0].x >= this.ui.gameContainer.width - 1 ||
            // Left wall
            this.snake[0].x <= -1 ||
            // Top wall
            this.snake[0].y >= this.ui.gameContainer.height - 1 ||
            // Bottom wall
            this.snake[0].y <= -1
        )
    }

    showGameOverScreen() {
        this.ui.gameOverScreen()
        this.ui.render()
    }

    tick() {
        if (this.isGameOver()) {
            this.showGameOverScreen()
            clearInterval(this.timer)
            this.timer = null

            return
        }

        this.changingDirection = false
        this.ui.clearScreen()
        this.drawDot()
        this.moveSnake()
        this.drawSnake()
        this.ui.render()
    }

    start() {
        if (!this.timer) {
            this.reset()

            this.timer = setInterval(this.tick.bind(this), 150)
        }
    }

    quit() {
        process.exit(0)
    }
}

module.exports = { Game }



// hi are you ready ?


// yes
// i have a cheated a little and get code from another place and rewrite it because i never wrote code for terminal

// are you here?
//yes
//SO u copy it all ?


// not all i just copied the part for terminal and snake moving
//so what part did you write ?
// the part of box and dots drawing
// and the logic for incrementing the score
//OK - please git the code and share it with me
// send it to my email


//ok
